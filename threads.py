
import threading
import time

def counts_up():
    count = 1
    while count<11:
        print("Thread 1 counts up: ", count )
        count += 1
        time.sleep(1)

def counts_down():
    count = 10
    while count>0:
        print("Thread 2 counts down: ", count )
        count -= 1
        time.sleep(1)
        
def main():
    thread_1 = threading.Thread(target = counts_up)
    thread_2 = threading.Thread(target = counts_down)
    thread_1.start()
    thread_1.join()
    thread_2.start()
    thread_2.join()

main()